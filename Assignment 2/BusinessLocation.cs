﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Assignment_2
{
    class BusinessLocation
    {
        public string businessName { get; set; }
        public string branchCode { get; set; }
        public int maximumCapacity { get; set; }
        public int visitorsNow { get; set; }

        public BusinessLocation() { }
        public BusinessLocation(string bn, string bc, int mc, int vn) 
        {
            businessName = bn;
            branchCode = bc;
            maximumCapacity = mc;
            visitorsNow = vn;
        }

        public override string ToString()
        {
            return
                "businessName:" + businessName +
                "\tbranchCode: " + branchCode +
                "\tmaximumCapacity:" + maximumCapacity +
                "\tvisitorsNow:" + visitorsNow;
        }
    }
}
