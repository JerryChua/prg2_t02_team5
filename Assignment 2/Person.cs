﻿//============================================================
// Student Number : S10194072, S10203077
// Student Name : Calvin Lee Wei Zhong, Chua Xuan Wei
// Module Group : T02 
//============================================================
using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment_2
{
    abstract class Person
    {
        public string Name { get; set; }
        public List<SafeEntry> SafeEntryList { get; set; }
        public List<TravelEntry> TravelEntryList { get; set; }

        public Person() 
        {
            TravelEntryList = new List<TravelEntry>();
            SafeEntryList = new List<SafeEntry>();
        }

        public Person(string n)
        {
            Name = n;
            TravelEntryList = new List<TravelEntry>();
            SafeEntryList = new List<SafeEntry>();
        }

        public void AddTravelEntry(TravelEntry TravelEntry)
        {
            TravelEntryList.Add(TravelEntry);
        }

        public void AddSafeEntry(SafeEntry SafeEntry)
        {
            SafeEntryList.Add(SafeEntry);
        }

        public abstract double CalculateSHNCharges();

        public override string ToString()
        {
            return
                "Name: " + Name;
        }
    }
}
