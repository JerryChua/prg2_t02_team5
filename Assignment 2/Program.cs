﻿//============================================================
// Student Number : S10194072, S10203077
// Student Name : Calvin Lee Wei Zhong, Chua Xuan Wei
// Module Group : T02 
//============================================================

using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace Assignment_2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<BusinessLocation> businessLocationList = new List<BusinessLocation>();
            List<Person> personList = new List<Person>();
            List<string> nameList = new List<string>();
            List<Visitor> visitorList = new List<Visitor>();
            List<Resident> residentList = new List<Resident>();
            List<TravelEntry> TravelEntryList = new List<TravelEntry>();
            List<SHNFacility> facilities = new List<SHNFacility>();
            List<string> fullList = new List<string>();


            using (StreamReader sr = new StreamReader("Person.csv"))
            {
                string s = sr.ReadLine();
                while ((s = sr.ReadLine()) != null)
                {
                    fullList.Add(s);
                }
            }

            foreach (var fl in fullList)
            {
                string[] info = fl.Split(',');
                if (info[0] == "visitor")                                           // can try if try catch here when have time
                {
                    visitorList.Add(new Visitor(info[1], info[4], info[5]));
                }
                else if (info[0] == "resident")
                {
                    residentList.Add(new Resident(info[1], info[2], Convert.ToDateTime(info[3])));
                }
            }

            foreach (Visitor v in visitorList)
            {
                personList.Add(v);
            }

            foreach (Resident r in residentList)
            {
                personList.Add(r);
            }

            using (StreamReader sr = new StreamReader("Person.csv"))
            {
                string s = sr.ReadLine();
                while ((s = sr.ReadLine()) != null)
                {
                    string[] info = s.Split(',');
                    nameList.Add(info[1]);
                }
            }



            using (StreamReader sr = new StreamReader("BusinessLocation.csv"))
            {
                string s = sr.ReadLine();
                while ((s = sr.ReadLine()) != null)
                {
                    string[] info = s.Split(',');
                    businessLocationList.Add(new BusinessLocation(info[0],
                        Convert.ToString(info[1]), Convert.ToInt32(info[2]), 0));
                }
            }


            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://covidmonitoringapiprg2.azurewebsites.net");
                Task<HttpResponseMessage> responseTask = client.GetAsync("/facility");
                responseTask.Wait();
                HttpResponseMessage result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    Task<string> readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();
                    string data = readTask.Result;
                    facilities = JsonConvert.DeserializeObject<List<SHNFacility>>(data);
                    {
                        foreach (SHNFacility s in facilities)
                        {
                            s.FacilityVacancy = s.FacilityCapacity;
                        }
                    }
                }
            }

            
            while (true)
            {
                Menu();
                Console.WriteLine("Enter your option: ");
                try
                {
                    int option = Convert.ToInt32(Console.ReadLine());
                    if (option == 1)
                    {
                        
                        NewVisitor(fullList);
                    }

                    if (option == 2)
                    {
                        TravelEntryRecord(fullList, personList, facilities, nameList);
                    }

                    if (option == 3)
                    {
                        DisplayFacil(facilities);
                    }

                    if (option == 4)
                    {
                        Charges(facilities, personList, fullList, nameList);
                    }

                    if (option == 5)
                    {
                        Console.Write("Enter Name:");
                        string name = Console.ReadLine();
                        foreach (var fl in fullList)
                        {
                            string[] info = fl.Split(',');
                            if (name == info[1])
                            {
                                if (info[0] == "visitor")
                                {
                                    Console.WriteLine("Name     | PassportNo | Nationality | Last Country | Entry By |    SHNStart     |      SHNEnd     |  Paid  | Facility      |");
                                    Console.WriteLine("{0,-8} | {1,-10} | {2,-11} | {3,-12} | {4,-8} | {5,-15} | {6,-15} | {7,-6} | {8,-13} |",
                                        info[1], info[4], info[5], info[9], info[10], info[11], info[12], info[13], info[14]);
                                }
                                else if (info[0] == "resident")
                                {
                                    Console.WriteLine("Name     | Address         | LastLeftCountry | TokenSN | Collection  | TokenExpiry | Last Country | Entry By |    SHNStart     |      SHNEnd     |  Paid  | Facility      |");
                                    Console.WriteLine("{0,-8} | {1,-15} | {2,-15} | {3,-7} | {4,-11} | {5,-11} | {6,-12} | {7,-8} | {8,-13} | {9,-13} | {10,-6} | {11,-13} |",
                                        info[1], info[2], info[3], info[6], info[7], info[8], info[9], info[10], info[11], info[12], info[13], info[14]);
                                }
                            }
                            else
                            {
                                continue;
                            }
                        }

                    }
                    if (option == 6)
                    {
                        Console.WriteLine("Name     | PassportNo | Nationality | Last Country | Entry By |    SHNStart     |      SHNEnd     |  Paid  | Facility      |");
                        Console.WriteLine("---------+------------+-------------+--------------+----------+-----------------+-----------------+--------+---------------+");
                        foreach (var fl in fullList)
                        {
                            string[] info = fl.Split(',');
                            if (info[0] == "visitor")                                           // can try if try catch here when have time
                            {
                                Console.WriteLine("{0,-8} | {1,-10} | {2,-11} | {3,-12} | {4,-8} | {5,-15} | {6,-15} | {7,-6} | {8,-13} |", info[1], info[4], info[5], info[9], info[10], info[11], info[12], info[13], info[14]);
                                Console.WriteLine("---------+------------+-------------+--------------+----------+-----------------+-----------------+--------+---------------+");
                            }
                        }
                    }
                    if (option == 0)
                    {
                        Console.WriteLine("EXITING...\nEXITED");
                        break;

                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Invalid Option!!!");
                }
            }




            
            static void Menu()
            {
                Console.WriteLine("---------------- M E N U --------------------");
                Console.WriteLine("[1] New Visitor");
                Console.WriteLine("[2] Travel Entry Record  [WIP]");
                Console.WriteLine("[3] View Facilities");
                Console.WriteLine("[4] Calculate SHN Charges");
                Console.WriteLine("[5] Person List");
                Console.WriteLine("[6] Visitor List");
                Console.WriteLine("[7] ");
                Console.WriteLine("[0] Exit");
                Console.WriteLine("---------------------------------------------");
            }

            
            static void NewVisitor(List<string> fullList)
            {
                Console.Write("Enter Name: ");
                string name = Console.ReadLine();
                Console.Write("Enter Passport Number: ");
                string pn = Console.ReadLine();
                Console.Write("Enter Nationality: ");
                string nat = Console.ReadLine();
                fullList.Add("visitor," + name + ",,," + pn + "," + nat + ",,,,,,,,,");
                Person p = new Visitor(name, pn, nat);
            }

            static void TravelEntryRecord(List<string> fullList, List<Person> personList, List<SHNFacility> facilities, List<string> nameList)
            {
                while (true)
                {
                    Console.Write("Enter Name:");
                    string name = Console.ReadLine();
                    Person travel = Search(name, personList);
                    if (travel == null)                                      //if name is null then there is no portion
                    {
                        Console.WriteLine("name not found!!\n");
                    }

                    else
                    {
                        try
                        {
                            Console.Write("Last Country of Embarkation: ");
                            string LOE = Console.ReadLine();
                            Console.Write("Entry Mode: ");
                            string EM = Console.ReadLine();
                            Console.Write("Enter Entry Date: ");
                            DateTime d = Convert.ToDateTime(Console.ReadLine());
                            foreach (var fl in fullList)
                            {
                                string[] info = fl.Split(',');
                                if (name == info[1])
                                {
                                    travel.AddTravelEntry(new TravelEntry(LOE, EM, d));
                                    TravelEntry te = travel.TravelEntryList[travel.TravelEntryList.Count - 1];
                                    te.CalculateSHNDuration();
                                    Console.WriteLine(te.SHNEndDate + " Days");
                                    if (LOE != "Macao SAR" || LOE != "Vietnam" || LOE != "New Zealand")
                                    {
                                        DisplayFacil(facilities);
                                        Console.Write("Select your facility: ");
                                        string facil = Console.ReadLine();
                                        for (int i = 0; i < facilities.Count; i++)
                                        {
                                            if ((facil == facilities[i].FacilityName))
                                            {
                                                Console.WriteLine("HERE");
                                                facilities[i].FacilityVacancy -= 1;
                                            }

                                        }
                                    }
                                }
                            }
                            break;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("\nInvalid input, Please try again!\n");
                        }

                    }
                }
            }

            static void Charges(List<SHNFacility> facilities, List<Person> personList, List<string> fullList, List<string> nameList)
            {
                Console.Write("Enter Name:");
                string name = Console.ReadLine();
                Person travel = Search(name, personList);
                if (travel == null)
                {
                    Console.WriteLine("name not found!!\n");
                }

                else
                {
                    try
                    {
                        //foreach (Visitor v in personList)
                        //{
                        //    foreach (TravelEntry te in p.TravelEntryList)
                        //    {
                        //        if ((te.SHNEndDate.Date < DateTime.Now) && te.IsPaid == false)
                        //        {
                        //            v.CalculateSHNCharges();
                        //        }
                        //    }
                        //}
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            static Person Search(string name, List<Person> personList)
            {
                for (int i = 0; i < personList.Count; i++)
                {
                    if (name == personList[i].Name)
                    {
                        return personList[i];
                    }
                }

                return null;
            }

            static void DisplayFacil(List<SHNFacility> facilities)
            {
                Console.WriteLine("Facility Name          Capacity     Vacancy");
                foreach (SHNFacility s in facilities)
                {
                    Console.WriteLine("{0,-20}   {1,-10}   {2,-10}  ",
                       s.FacilityName, s.FacilityCapacity, s.FacilityVacancy);
                }
            }

            //AssignReplaceTraceTogetherToken(personList)

            static void AssignReplaceTraceTogetherToken(List<Person> personList)
            {
                TraceTogetherToken t = new TraceTogetherToken();
                t.ReplaceToken("395462", "ACE The Place CC");
            }

            //ListAllBusinessLocations(businessLocationList)

            static void ListAllBusinessLocations(List<BusinessLocation> businessLocationList)
            {
                Console.WriteLine("{0,-25} {1, -13} {2, -18}",
                   "businessname", "branchCode", "maximumcapacity");
                for (int g = 0; g < businessLocationList.Count; g++)
                {
                    Console.WriteLine("{0,-25} {1, -13} {2, -18}",
                        businessLocationList[g].businessName, businessLocationList[g].branchCode,
                        businessLocationList[g].maximumCapacity);
                }
            }

            //EditBusinessLocationCapacity(businessLocationList)

            static void EditBusinessLocationCapacity(List<BusinessLocation> businessLocationList)
            {
                Console.Write("Enter business name: ");
                string busName = Console.ReadLine();
                Console.Write("Enter branch code: ");
                string branchNo = Console.ReadLine();
                for (int r = 0; r < businessLocationList.Count; r++)
                {
                    if (businessLocationList[r].businessName == busName && businessLocationList[r].branchCode == branchNo)
                    {
                        Console.Write("Enter new maximum capacity: ");
                        businessLocationList[r].maximumCapacity = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("New maximum capacity: " + businessLocationList[r].maximumCapacity);

                    }
                }
            }

            // SafeEntryCheckIn(businessLocationList, personList)

            static void SafeEntryCheckIn(List<BusinessLocation> businessLocationList, List<Resident> residentList, List<Visitor> visitorList)
            {
                Console.Write("Enter person name: ");
                string name = Console.ReadLine();
                using (StreamReader sr = new StreamReader("Person.csv"))
                {
                    string s = sr.ReadLine();
                    while ((s = sr.ReadLine()) != null)
                    {
                        string[] s2 = s.Split(',');
                        if (name == s2[1])
                        {
                            ListAllBusinessLocations(businessLocationList);
                            Console.Write("Enter business name to check in: ");
                            string BusName = Console.ReadLine();
                            foreach (BusinessLocation b in businessLocationList)
                            {
                                if (b.businessName == BusName && b.visitorsNow < b.maximumCapacity)
                                {
                                    if (s2[0] == "resident")
                                    {
                                        foreach (Resident r in residentList)
                                        {
                                            if (r.Name == name)
                                            {
                                                r.AddSafeEntry(new SafeEntry(DateTime.Now,
                                                    Convert.ToDateTime("31-Dec-9999"), b));
                                                b.visitorsNow += 1;
                                            }
                                            Console.WriteLine("Check-in complete.");
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        foreach (Visitor v in visitorList)
                                        {
                                            if (v.Name == name)
                                            {
                                                v.AddSafeEntry(new SafeEntry(DateTime.Now,
                                                    Convert.ToDateTime("31-Dec-9999"), b));
                                                b.visitorsNow += 1;
                                            }
                                            Console.WriteLine("Check-in complete.");
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //SafeEntryCheckOut(personList)

            static void SafeEntryCheckOut(List<Resident> residentList, List<Visitor> visitorList)
            {
                Console.Write("Enter person name: ");
                string name = Console.ReadLine();
                using (StreamReader sr = new StreamReader("Person.csv"))
                {
                    string s = sr.ReadLine();
                    while ((s = sr.ReadLine()) != null)
                    {
                        string[] s2 = s.Split(',');
                        if (s2[1] == name)
                        {
                            if (s2[0] == "resident")
                            {
                                foreach (Resident r in residentList)
                                {
                                    if (s2[1] == r.Name)
                                    {
                                        Console.WriteLine(r.SafeEntryList.Count);
                                        for (int r2 = 0; r2 < r.SafeEntryList.Count; r2++)
                                        {
                                            Console.WriteLine(r.SafeEntryList[r2]);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                foreach (Visitor v in visitorList)
                                {
                                    if (s2[1] == v.Name)
                                    {
                                        Console.WriteLine(v.SafeEntryList.Count);
                                        for (int v2 = 0; v2 < v.SafeEntryList.Count; v2++)
                                        {
                                            Console.WriteLine(v.SafeEntryList[v2]);
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }
    }
}