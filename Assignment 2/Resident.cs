﻿//============================================================
// Student Number : S10194072, S10203077
// Student Name : Calvin Lee Wei Zhong, Chua Xuan Wei
// Module Group : T02 
//============================================================
using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment_2
{
    class Resident : Person
    {
        public string Address { get; set; }
        public DateTime LastLeftCountry { get; set; }
        public TraceTogetherToken Token { get; set; }

        public Resident() { }

        public Resident(string n, string a, DateTime llc) : base(n)
        {
            Address = a;
            LastLeftCountry = llc;
        }

        public override double CalculateSHNCharges()
        {
            return 69;
        }
        
        public override string ToString()
        {
            return base.ToString() + "\tAddress: " + Address + "\tLast Left Country: "
                + LastLeftCountry + "\tToken: " + Token;
        }
    }
}
