﻿//============================================================
// Student Number : S10194072, S10203077
// Student Name : Calvin Lee Wei Zhong, Chua Xuan Wei
// Module Group : T02 
//============================================================
using System;

namespace Assignment_2
{
    class SHNFacility
    {
        public string FacilityName { get; set; }
        public int FacilityCapacity { get; set; }
        public int FacilityVacancy { get; set; }
        public double DistFromAirCheckpoint { get; set; }
        public double DistFromSeaCheckpoint { get; set; }
        public double DistFromLandCheckpoint { get; set; }

        public SHNFacility() { }
        public SHNFacility(string n, int c, double DA, double DS, double DL)
        {
            FacilityName = n;
            FacilityCapacity = c;
            DistFromAirCheckpoint = DA;
            DistFromLandCheckpoint = DL;
            DistFromSeaCheckpoint = DS;
        }

        public bool IsAvaliable()
        {
            if (FacilityVacancy < 1)
            {
                return false;
            }
            else
            {
                return true;
            }
            
        }
        public double CalculateTravelCost(string EM, DateTime d)        //so this is all wrong
        {
            double distance = 0;
            double fare = 0;
            if (EM == "Air")
            {
                distance = DistFromAirCheckpoint;
            }
            else if (EM == "Sea")
            {
                distance = DistFromSeaCheckpoint;
            }
            else if (EM == "Land")
            {
                distance = DistFromLandCheckpoint;
            }
            fare = 50 + distance * 0.22;
            if (d.Hour > 0 && d.Hour < 6)
            {
                return fare * 1.5;
            }
            else if (d.Hour > 9 && d.Hour < 18)
            {
                return fare;
            }
            else
            {
                return fare * 1.25;
            }
        }
    }
}
