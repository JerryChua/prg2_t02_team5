﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment_2
{
    class SafeEntry
    {
        public DateTime checkIn { get; set; }
        public DateTime checkOut { get; set; }

        public BusinessLocation location { get; set; }

        public SafeEntry()
        {

        }

        public SafeEntry(DateTime getin, DateTime getout, BusinessLocation bl)
        {
            checkIn = getin;
            checkOut = getout;
            location = bl;
        }

        public void PerformCheckOut()
        {

        }

        public override string ToString()
        {
            return
                "checkIn:" + checkIn +
                "\tcheckOut:" + checkOut +
                "\tlocation:" + location;
        }
    }
}
