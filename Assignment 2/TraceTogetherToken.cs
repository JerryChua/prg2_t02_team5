﻿// come back here later

//============================================================
// Student Number : S10194072, S10203077
// Student Name : Calvin Lee Wei Zhong, Chua Xuan Wei
// Module Group : T02 
//============================================================


using System;
using System.Collections.Generic;
using System.IO;

namespace Assignment_2
{
    class TraceTogetherToken
    {
        public string serialNo { get; set; }
        public string collectionLocation { get; set; }
        public DateTime expiryDate { get; set; }

        public TraceTogetherToken() { }

        public TraceTogetherToken(string s, string c, DateTime e)
        {
            serialNo = s;
            collectionLocation = c;
            expiryDate = e;
        }

        public bool IsEligibleForReplacement()
        {
            if (DateTime.Now < (expiryDate.AddMonths(1)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void ReplaceToken(string s, string c)
        {
            Console.Write("Enter user's name: ");
            string name = Console.ReadLine();
            TraceTogetherToken ttt1;
            using (StreamReader sr = new StreamReader("Person.csv"))
            {
                string k = sr.ReadLine();
                while ((k = sr.ReadLine()) != null)
                {
                    string[] k2 = k.Split(",");
                    if (k2[1] == name && k2[0] == "resident")
                    {
                        try
                        {
                            expiryDate = Convert.ToDateTime(k2[8]);
                            if (IsEligibleForReplacement())
                            {
                                Console.WriteLine("Token can be replaced.");
                                break;
                            }
                            else
                            {
                                Console.WriteLine("Token can no longer be replaced.");
                                break;
                            }
                        }
                        catch
                        {
                            Console.WriteLine("New token shall be assigned.");
                            ttt1 =
                                new TraceTogetherToken(s, c, DateTime.Now.AddMonths(6));
                            break;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Must be resident!");
                    }
                }
            }
        }
        
        public override string ToString()
        {
            return 
                "serialNo:" + serialNo + "\tcollectionLocation:"
                + collectionLocation + "\texpiryDate:" + expiryDate;
        }
        

    }
}
