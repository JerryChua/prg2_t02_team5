﻿//============================================================
// Student Number : S10194072, S10203077
// Student Name : Calvin Lee Wei Zhong, Chua Xuan Wei
// Module Group : T02 
//============================================================
using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment_2
{
    class TravelEntry
    {
        public string LastCountryOfEmbarkation { get; set; }
        public string EntryMode { get; set; }
        public DateTime EntryDate { get; set; }
        public DateTime SHNEndDate { get; set; }
        public SHNFacility ShnStay { get; set; }
        public bool IsPaid { get; set; }

        public TravelEntry() { }
        public TravelEntry(string LOE, string EM, DateTime ED)
        {
            LastCountryOfEmbarkation = LOE;
            EntryMode = EM;
            EntryDate = ED;
        }

        public void CalculateSHNDuration()
        {
            if (LastCountryOfEmbarkation == "New Zealand" || LastCountryOfEmbarkation == "Vietnam")
            {
                SHNEndDate = EntryDate;
            }
            else if (LastCountryOfEmbarkation == "Macao SAR")
            {
                SHNEndDate = EntryDate.AddDays(7);
            }
            else
            {
                SHNEndDate = EntryDate.AddDays(14);
            }
        }

        public void AssignSHNfacility(SHNFacility facilities)
        {
            
        }
        public override string ToString()
        {
            return "Last country of embarkation" + LastCountryOfEmbarkation + "Entry Mode" + EntryMode + "Entry Date" + EntryDate ;
        }
    }
}
