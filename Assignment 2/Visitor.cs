﻿//============================================================
// Student Number : S10194072, S10203077
// Student Name : Calvin Lee Wei Zhong, Chua Xuan Wei
// Module Group : T02 
//============================================================
using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment_2
{
    class Visitor : Person
    {
        public string PassportNo { get; set; }
        public string Nationality { get; set; }

        public Visitor(string n, string p, string na) : base(n)
        {
            PassportNo = p;
            Nationality = na;
        }

        public override double CalculateSHNCharges()
        {
            return 1000000;
            /*
            double price = p;
            foreach (Visitor v in personList)
            {
                foreach (TravelEntry te in v.TravelEntryList)
                {
                    if (te.LastCountryOfEmbarkation == "New Zealand" || te.LastCountryOfEmbarkation == "Vietnam" || te.LastCountryOfEmbarkation == "Macao SAR")
                    {
                        return 200 + 80;
                    }
                    else
                    {
                        return price + 2000;                                        //come back to this later
                    }
                    {
                        Console.Write("Enter Entry Mode: ");
                        string EM = Console.ReadLine();
                        Console.Write("Enter Entry Date");
                        DateTime d = Convert.ToDateTime(Console.ReadLine()); 
                        return price + te.ShnStay.CalculateTravelCost(EM,d);
                    }
                }
            }
            */
        }

        public override string ToString()
        {
            return base.ToString() + "\tPassport Number: " + PassportNo + "\tNationality: "
                + Nationality;
        }
    }
}
